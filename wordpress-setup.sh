#!/bin/bash
# Wordpress install script
# Written by Joshua Mudge
# Ad Mejorem Dei Glorium
version="v1.1.0"

curl -s "https://gitlab.daplie.com/sysadmin/daplie-snippets/raw/master/dss/setup.sh" | bash

dss --user $USER init

#sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8

sudo apt-get install nginx unzip php-fpm php-xml php-xmlrpc software-properties-common php-mysql mysql-server zip php-gd php-bcmath php-curl php-imagick -y
wget https://wordpress.org/latest.zip
sudo mkdir /var/www/wordpress
unzip latest.zip
sudo mv wordpress/* /var/www/website/

sudo add-apt-repository -y ppa:certbot/certbot
sudo apt-get update
sudo apt-get install -y python-certbot-nginx certbot

# user input required

echo "Update Nginx configuration in /etc/nginx/sites-enabled/ using example configuration."
echo "run: sudo certbot --authenticator webroot --installer nginx  after configuring default_server and root directory. You will have to input the server root directory."
# You could also run: sudo certbot certonly --webroot --webroot-path=/var/www/wordpress -d www.youdomain.com -d yourdomain.com"
# For permalinks, add: try_files $uri $uri/ /index.php?$args; in place of the other try_files in configuration.
# Fix perms sudo chown -R www-data:www-data /var/www/dir

#Run: sudo mysql_secure_installation

# Change Table prefix in interface if you have multiple sites.
# mysql -u root mysql -p
#CREATE DATABASE wordpress;
#CREATE USER 'wordpress'@'localhost' IDENTIFIED BY 'newpassword';
#GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'localhost';
#FLUSH PRIVILEGES;
#quit


# Install WP-CLI

curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

#https://askubuntu.com/questions/705458/ubuntu-15-10-mysql-error-1524-unix-socket

#https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

# Create root crontab: 0 5 20 * * sudo letsencrypt renew --force-renew && sudo service nginx restart

# Crontab for BASH autoupdate: * * * * 0 bash "cd /var/www/wordpress && wp core update && wp plugin update --all && wp theme update --all"
# Make sure you get both default and default80 configs setup and have /var/www/wordpress setup properly and not at /var/www/wordpress/wordpress/
# WP-CLI
