cd 2017
sermons=$(ls /mnt/52265C99265C8041/Users/Berean-Sound/sermons)
IFS=$'\n'
lastsermon=$(echo "${sermons[*]}" | sort -nr | head -n1)
cp /mnt/52265C99265C8041/Users/Berean-Sound/sermons/$lastsermon .

rsync --update --progress -e "ssh -i /home/berean/sermons/.tempssh/temp" *.mp3 sermons@bereanbibleutah.org:/home/josh/sermons/2017/

sermons=$(ssh -i /home/berean/sermons/.tempssh/temp sermons@bereanbibleutah.org 'ls /home/josh/sermons/2017')
IFS=$'\n'
recentsermon=$(echo "${sermons[*]}" | sort -nr | head -n1)
echo $recentsermon

#cd ..
#cd 2016
#rsync --progress -e "ssh -i ../.tempssh/temp" *.mp3 sermons@bereanbibleutah.org:/home/josh/sermons/2016/